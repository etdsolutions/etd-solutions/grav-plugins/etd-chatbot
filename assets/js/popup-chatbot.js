var popupChatbot = window.popupChatbot = {

    $content: null,
    container: '',

    init: function (content, container) {

        this.$content = content;
        this.container = container.toString();

        this.bind();
    },
    bind: function () {

        var self = this;

        $('body').append(this.$content);

        $('.level-2').hide();

        $('.level-1').on('click', function(e){
           $('.level-1').hide();
            let $this = e.currentTarget;
            let parent = $this.parentNode;

            let children = parent.getElementsByClassName('level-2');

            $.each(children, function(index, value){

                $(value).show();
            });
        });

        $('.back-level-1').on('click', function(){
            $('.level-1').show();
            $('.level-2').hide();
        })

    }

}