<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use Grav\Common\Utils;

/**
 * Class ChatbotPlugin
 * @package Grav\Plugin
 */
class EtdChatbotPlugin extends Plugin
{
    static $plugin_config;

    /**
     * Init chatbot template.
     * @type string
     */
    private $initTemplate = 'partials/etd-chatbot.html.twig';


    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        // Enable the main event we are interested in
        $this->enable([
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0]
        ]);

        self::$plugin_config = $this->config();

    }

    /**
     * Push plugin templates to twig paths array.
     */
    public function onTwigTemplatePaths()
    {
        // Push own templates to twig paths
        array_push(
            $this->grav['twig']->twig_paths,
            __DIR__ . '/templates'
        );
    }

    public function onTwigSiteVariables()
    {
        $chatbot = isset($_COOKIE['chatbot']) ? (string) $_COOKIE['chatbot'] : '';
        $config = self::$plugin_config;

        if(isset($config['enabled']) && $config['enabled'] == 1){
            if ($chatbot !== "ok") {

                $this->grav['assets']->addJs('plugin://etd-chatbot/assets/js/popup-chatbot.js', '', 'bottom');

                $twig = $this->grav['twig'];
                $this->grav['assets']->addInlineJs($twig->twig->render('partials/etd-chatbot.html.twig', ['configChatbot' => $config]), '', 'bottom');
            }
        }



    }



}

